// Implementation of a gap buffer.
//
// Important: GapBuffer uses two types of indices: text indexes and underlying indices which are
// indices into the array which includes the gap. For example the text "Hello world" would have text
// indices [0,10] and (assuming a gap size of 4, e.g: "Hello ____world") underlying indixes
// [0,14]. User facing functions use text indexing unless started otheswise.
//
// TODO:
//  - Consider reducing gap size (if bigger than text - min chunk size)
//  - Resizing strategy (double the chunk?)

package gapbuffer

import (
	"fmt"

	"gitlab.com/onikolas/math"
)

// Gap buffer https://en.wikipedia.org/wiki/Gap_buffer
type GapBuffer struct {
	buffer           []rune // storage
	gapStart, gapEnd int    // gap indices
	cursor           int    // current position to add text to
	resizeChunk      int    // how many entries to add when adding more space
}

const DefaultChunkSize = 256

func NewGapBuffer(size int) *GapBuffer {
	gb := &GapBuffer{}
	gb.Init(size)
	return gb
}

// Initialize a Gap to given size
func (gb *GapBuffer) Init(size int) {
	gb.buffer = make([]rune, size)
	gb.cursor, gb.gapStart = 0, 0
	gb.gapEnd = size
	gb.resizeChunk = DefaultChunkSize
}

// Set how much space to add when buffer becomes full
func (gb *GapBuffer) SetResizeChunk(c int) {
	if c < 1 {
		c = 1
	}
	gb.resizeChunk = c
}

// Text returns slices with all of the stored text. Returns two slices into the underlying array
// to avoid copying and concatenation. Modifying the slices is not recomended especially if the
// buffer is modified between the call to Text and the modification.
func (gb GapBuffer) Text() ([]rune, []rune) {
	return gb.buffer[0:gb.gapStart], gb.buffer[gb.gapEnd:]
}

// Insert adds text to the buffer at text index pos. We allow pos=gb.Lenght() which inserts after
// the exiting text. Simillarly, Insert accepts pos=0 to add text in an empty buffer.
func (gb *GapBuffer) Insert(text []rune, pos int) bool {
	if len(text) > gb.GapSize() {
		if gb.resizeChunk < len(text) {
			gb.resizeChunk = len(text)
		}
		gb.Resize(gb.resizeChunk)
	}

	if !gb.validPos(pos) {
		return false
	}

	// move the gap if needed
	if pos != gb.gapStart {
		gb.moveGap(pos)
	}

	for i := range text {
		gb.buffer[pos+i] = text[i]
	}
	gb.gapStart += len(text)
	gb.cursor = gb.gapStart
	return true
}

// InsertAtCursor adds text at the cursor position
func (gb *GapBuffer) InsertAtCursor(text []rune) bool {
	if gb.cursor != gb.gapStart {
		gb.moveGap(gb.cursor)
	}
	return gb.Insert(text, gb.cursor)
}

// Move the gap to pos. Will cause shifting proportional to the movement: O(pos-gapStart)
func (gb *GapBuffer) moveGap(pos int) bool {

	if pos < 0 || pos > len(gb.buffer)-gb.GapSize() {
		return false
	}

	move := pos - gb.gapStart
	newS := pos
	newE := pos + gb.GapSize()

	if move < 0 {
		absmove := math.Abs(move)
		for i := 0; i < absmove; i++ {
			gb.buffer[gb.gapEnd-1-i] = gb.buffer[newS+absmove-1-i]
		}
	} else {
		for i := 0; i < move; i++ {
			gb.buffer[gb.gapStart+i] = gb.buffer[gb.gapEnd+i]
		}
	}

	gb.gapStart = newS
	gb.gapEnd = newE

	return true
}

// Resize changes the size of the gap. Positive values increase and negative values decrease. Will
// not delete text - only the gap can be reduced.
func (gb *GapBuffer) Resize(size int) {

	// check valid size - cannot deacrease below length of text
	if gs := gb.GapSize(); size < 0 && math.Abs(size) > gs {
		size = -gs
	}

	newBuffer := make([]rune, len(gb.buffer)+size)

	for i := 0; i < gb.gapStart; i++ {
		newBuffer[i] = gb.buffer[i]
	}
	for i := gb.gapEnd; i < len(gb.buffer); i++ {
		newBuffer[i+size] = gb.buffer[i]
	}

	gb.buffer = newBuffer
	gb.gapEnd += size
}

// DeleteBack deletes a num of characters behind the cursor
func (gb *GapBuffer) DeleteBack(num uint) {
	if gb.cursor != gb.gapStart {
		gb.moveGap(gb.cursor)
	}
	gb.gapStart -= int(num)
	if gb.gapStart < 0 {
		gb.gapStart = 0
	}
	gb.cursor = gb.gapStart
}

// DeleteForward deletes a num of characters in front of the cursor
func (gb *GapBuffer) DeleteForward(num uint) {
	if gb.cursor != gb.gapStart {
		gb.moveGap(gb.cursor)
	}
	gb.gapEnd += int(num)
	if gb.gapEnd >= len(gb.buffer) {
		gb.gapEnd = len(gb.buffer) - 1
	}
}

// DeleteAll clears all text by setting the gap to cover all of the underlying array. It is a
// constant time operation, but it cannot alter the size of underlying buffer (use Resize for that)
func (gb *GapBuffer) DeleteAll() {
	gb.cursor, gb.gapStart, gb.gapEnd = 0, 0, len(gb.buffer)
}

// Is the position valid for inserting text
func (gb *GapBuffer) validPos(pos int) bool {
	return pos >= 0 && pos <= gb.Length()
}

// GapSize returns the size of the gap. This is the max length of text that can be inserted before a reallocation occurs
func (gb *GapBuffer) GapSize() int {
	return gb.gapEnd - gb.gapStart
}

// String implementation for Stringer. Does not print contents of gap.
func (gb GapBuffer) String() string {
	return string(gb.buffer[0:gb.gapStart]) + string(gb.buffer[gb.gapEnd:])
}

// Debug buffer print. Prints gap contents as well as the stored string.
func (gb GapBuffer) DebugString() string {
	return fmt.Sprint(string(gb.buffer), "\n", gb.cursor, gb.gapStart, gb.gapEnd)
}

// Length of the stored text.
func (gb GapBuffer) Length() int {
	return len(gb.buffer) - gb.GapSize()
}

// Return rune at position i.
func (gb GapBuffer) At(i int) rune {
	if i < 0 || i >= gb.Length() {
		return '\000'
	}
	if i < gb.gapStart {
		return gb.buffer[i]
	} else {
		return gb.buffer[i+gb.GapSize()]
	}
}

// Get a region of text defined by two indices [a,b). Unlike Regions, text is copied into the returned array.
func (gb GapBuffer) Region(a, b int) []rune {
	u, v := gb.Regions(a, b)
	text := make([]rune, len(u)+len(v))
	copy(text, u)
	copy(text[len(u):], v)
	return text
}

// Get a region of text defined by two indices [a,b). Returns two slices into the underlying array
// to avoid copying and concatenation. Modifying the slices is not recomended especially if the
// buffer is modified between the call to Regions and the modification.
func (gb GapBuffer) Regions(a, b int) ([]rune, []rune) {
	a = gb.ConstrainIndex(a)
	if b > gb.Length() {
		b = gb.Length()
	}
	if a < gb.gapStart {
		if b <= gb.gapStart {
			return gb.buffer[a:b], []rune{}
		}
		return gb.buffer[a:gb.gapStart], gb.buffer[gb.gapEnd : gb.gapEnd+b-gb.gapStart]
	}
	return []rune{}, gb.buffer[a+gb.GapSize() : b+gb.GapSize()]
}

// Contrain an index to [0, textLength) so that it always points to a valid text position.
func (gb GapBuffer) ConstrainIndex(index int) int {
	return math.Bound(index, 0, gb.Length()-1)
}

// Cursor returns the current cursor position. The cursor can be at gb.Length() to allow appending
// text at the Cursor position
func (gb GapBuffer) Cursor() int {
	return gb.cursor
}

// Move the cursor. Returns true if sucessful
func (gb *GapBuffer) MoveCursor(pos int) bool {
	if pos < 0 || pos > len(gb.buffer)-gb.GapSize() {
		return false
	}
	gb.cursor = pos
	return true
}

// HasNewLine returns true if there is a newline in this buffer. Used to detect input
// from buffers used for temporary user input (text promts, line editor etc)
func (gb *GapBuffer) HasNewLine() bool {
	for i := 0; i < gb.Length(); i++ {
		if gb.At(i) == '\n' {
			return true
		}
	}
	return false
}

// FindRuneBackwards scans the text backwards starting at  startIndex and returns the index of
// the first r it finds. If no match is found the index will be -1 and the flag false
func (gb *GapBuffer) FindRuneBackwards(r rune, startIndex int) (int, bool) {
	i := startIndex
	for ; i >= 0; i-- {
		if gb.At(i) == r {
			break
		}
	}
	return i, i > 0
}

// Find scans the text starting at startIndex and returns the index of the first r it
// finds. If no match is found the index will be gb.Length() and the flag false
func (gb *GapBuffer) FindRune(r rune, startIndex int) (int, bool) {
	i := startIndex
	for ; i < gb.Length(); i++ {
		if gb.At(i) == r {
			break
		}
	}
	return i, i < gb.Length()
}
