# GapBuffer
A simple [gap buffer](https://en.wikipedia.org/wiki/Gap_buffer) implementation. The implementation automatically resizes when the underlying array is full. 

```
package main

import (
	"fmt"

	"gitlab.com/onikolas/gapbuffer"
)

func main() {

	gb := gapbuffer.GapBuffer{}
	gb.InsertAtCursor([]rune("Hello!"))
	gb.MoveCursor(gb.Cursor() - 1)
	gb.InsertAtCursor([]rune(", world"))
	fmt.Println(gb.String())
	//Hello, world!
}

```
